'use strict';

angular.module('myApp.view1', ['ngRoute', 'ngAnimate', 'ngMaterial'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.
  when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  }).
  when('/view1/:imgId', {
    templateUrl: 'view1/image.html',
    controller: 'ImageCtrl'
  }); 
}])

.controller('View1Ctrl', ['$scope', '$timeout', '$mdDialog', function($scope, $timeout, $mdDialog) {

	$scope.slides = [
		{ 'image': '/img/im1.jpg' },
		{ 'image': '/img/im2.jpg' },
		{ 'image': '/img/im3.jpg' }
	];
	$scope.slideIndex = 0;

    $scope.menu = true;
    $scope.showMenu = function(menu){
        $scope.menu = !menu;
    }

    $scope.working = true;
    $scope.showWorking = function(working){
        $scope.working = !working;
    }

    $scope.address = true;
    $scope.showaddress = function(address){
        $scope.address = !address;
    }

	$scope.next = function() {
	     var total = $scope.slides.length; 
	     if (total > 0) {
		 $scope.slideIndex = ($scope.slideIndex == total - 1) ? 0 : $scope.slideIndex + 1;
	     }
	}; 
	$scope.play = function() {
	     $scope.timeOut = $timeout(function() {
		 $scope.next();
		 $scope.play();
	     }, 4000);
	};

	$scope.play();

	  $scope.openModal = function(ev, slide_index) { 
	    $mdDialog.show({
	      controller: DialogController,
	      template: '<md-dialog aria-label="List dialog">' +
            	    '<md-toolbar>'+
            	    '<div class="md-toolbar-tools">'+
            		'<h2>Image</h2>'+
            		'<span flex></span>'+
            		'<md-button class="md-icon-button" ng-click="cancel()">'+
            		'<md-icon md-svg-src="img/icons/ic_close_24px.svg" aria-label="Close dialog"></md-icon>'+
            		'</md-button>'+
            	    '</div>'+
            	    '</md-toolbar>'+
            	    '<md-dialog-content>'+ 
                    '<h1 class="h1_number_element"> Номер элемента - '+ (slide_index+1) + '</h1>'+
            	    '<img alt="img" class="modal-img" src="/img/im'+ (slide_index+1) + '.jpg">' +
                    '  </md-dialog-content>' +
                    '  <md-dialog-actions>' +
                    '    <md-button ng-click="cancel()" class="md-primary">' +
                    '      Close Dialog' +
                    '    </md-button>' +
                    '  </md-dialog-actions>' +
                    '</md-dialog>',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:true,
	      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
	    })
	  };

	  function DialogController($scope, $mdDialog) {

	    $scope.hide = function() {
	      $mdDialog.hide();
	    };
	    $scope.cancel = function() {
	      $mdDialog.cancel();
	    };
	    $scope.answer = function(answer) {
	      $mdDialog.hide(answer);
	    };
	  };

     $scope.products = [
         {
             img: '../img/products/hepcinat-sofosbuvir-400-mg--300x300.png',
             price: '$34.95',
             name: 'Hepcinat 400 mg'
         },
         {
             img: '../img/products/layer-10.png',
             price: '$34.95',
             name: 'Hepcinat Lp 90/400 mg'
         },
         {
             img: '../img/products/hepcinat-sofosbuvir-400-mg--300x300.png',
             price: '$34.95',
             name: 'Hepcinat 400 mg'
         },
         {
             img: '../img/products/layer-10.png',
             price: '$34.95',
             name: 'Hepcinat Lp 90/400 mg'
         },
         {
             img: '../img/products/layer-13.png',
             price: '$34.95',
             name: 'Hepcinat 400 mg'
         },
         {
             img: '../img/products/layer-10.png',
             price: '$34.95',
             name: 'Hepcinat Lp 90/400 mg'
         },
         {
             img: '../img/products/hepcinat-sofosbuvir-400-mg--300x300.png',
             price: '$34.95',
             name: 'Hepcinat 400 mg'
         },
         {
             img: '../img/products/layer-10.png',
             price: '$34.95',
             name: 'Hepcinat Lp 90/400 mg'
         }
     ];
//------------------------------------------------------------------modal
    var originatorEv;
    $scope.openMenu = function($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
    };
    $scope.notificationsEnabled = true;
    $scope.toggleNotifications = function() {
        this.notificationsEnabled = !$scope.notificationsEnabled;
    };
    $scope.redial = function() {
        $mdDialog.show(
            $mdDialog.alert()
                .targetEvent(originatorEv)
                .clickOutsideToClose(true)
                .parent('body')
                .title('Suddenly, a redial')
                .textContent('You just called a friend; who told you the most amazing story. Have a cookie!')
                .ok('That was easy')
        );
        originatorEv = null;
    };
    $scope.checkVoicemail = function() {
        // This never happens.
    };

}]);
