'use strict';

angular.module('myApp.view1')

.controller('ImageCtrl', ['$scope',  '$routeParams', function($scope, $routeParams) {
 
	$scope.img_id = $routeParams.imgId; 
	$scope.src = '/img/im'+ $scope.img_id +'.jpg'; 
}]);
